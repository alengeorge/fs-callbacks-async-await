const { removeAllJSONs, createDirectory, createJSON } = require("../problem1");


async function* fileGenerator(dirName,filenames) {
    for (let i = 0; i < filenames.length; i++) {
       yield await createJSON(dirName,filenames[i]);
    }
}

async function createDelete(dirName, ...filenames) {
    await createDirectory(dirName);
    let fileGen = fileGenerator(dirName,filenames)
  
    fileGen.next()
    fileGen.next()    
  

  await removeAllJSONs(dirName)

}


createDelete("Json", "a.json", "b.json");


// async function createDelete(dirName, ...filenames) {
//       await createDirectory(dirName);
//     let value2
//     for (let i = 0; i < filenames.length; i++) {
//         value2= await createJSON(dirName,filenames[i]);
//          console.log(value2)
//     }

//     await removeAllJSONs(dirName)
  
// }